from itertools import groupby
import json
import csv
from pathlib import Path

# %matplotlib inline 

import pandas as pd
import gensim, logging
import logging

from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
import re
import matplotlib.pyplot as plt

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

f_user_skill = Path(__file__).parent.parent.absolute() / 'dataset' /  'skill2vec' / 'user_skill.json'
f_job_skill = Path(__file__).parent.parent.absolute() / 'dataset' /  'skill2vec' / 'job_skill.json'
f_vietnamwork =  Path(__file__).parent.absolute() / 'dataset.csv'


def load_dataset_skill2vec():
    datasets = [f_user_skill, f_job_skill]
    res = []
    
    for dataset in datasets:
        with open(dataset, mode='r', encoding='utf-8') as f:
            data = json.load(f)
    res.extend(data)
    transactions = convert_to_transaction(res)
    return transactions

def load_dataset_vietnamwork():
    res = []
    with open(f_vietnamwork, mode='r', encoding='utf-8') as csvfile:
        data = csv.reader(csvfile, delimiter=',')
        for doc in data:
            print(doc)
            res.append(doc)
    return res

def convert_to_transaction(data):
    # define a fuction for key
    def key_func(k):
        return k['item']
    data = sorted(data, key=key_func)
    transactions = []
    for key, value in groupby(data, key_func):
        print(key)
        transaction = [i["value"] for i in value]
        print(transaction)
        transactions.append(transaction)
    return transactions

def training():
    transactions = load_dataset_vietnamwork()
    model = gensim.models.Word2Vec(transactions, min_count=1, workers=4, vector_size=200, window=20, sg=1)
    model.save("word2vec_vietnamworks.model")
    
    # repair data for visualize

    a = model.wv.most_similar('SQL')
    print(a)



training()
# load_dataset_vietnamwork()