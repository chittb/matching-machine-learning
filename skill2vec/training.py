from itertools import groupby
import json
import csv
from pathlib import Path
import webbrowser

# %matplotlib inline 

import pandas as pd
import gensim, logging
import logging

from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
import re
import matplotlib.pyplot as plt

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

def load_dataset():
    f_user_skill = Path(__file__).parent.absolute() / 'user_skill.json'
    f_job_skill = Path(__file__).parent.absolute() / 'job_skill.json'
    datasets = [f_user_skill, f_job_skill]
    res = []
    
    for dataset in datasets:
        with open(dataset, mode='r', encoding='utf-8') as f:
            data = json.load(f)
    res.extend(data)
    transactions = convert_to_transaction(res)
    return transactions

def convert_to_transaction(data):
    # define a fuction for key
    def key_func(k):
        return k['item']
    data = sorted(data, key=key_func)
    transactions = []
    for key, value in groupby(data, key_func):
        print(key)
        transaction = [i["value"] for i in value]
        print(transaction)
        transactions.append(transaction)
    return transactions

def training():
    transactions = load_dataset()
    model = gensim.models.Word2Vec(transactions, min_count=1, workers=4, vector_size=200, window=20, sg=1)
    model.save("skill2vec.model")

def most_similar(item):
    model = gensim.models.Word2Vec.load("skill2vec.model")
    similar_items = model.wv.most_similar(str(item))
    return similar_items

def recursive_get_similar_items(keyword, cur_score, cur_dept, max_dept):
    print(keyword)
    res = {
        "name": str(keyword),
        "data": {
            "score": cur_score
        },
        "children": []
    }
    if cur_dept == max_dept:
        return res

    cur_dept += 1
    similar_items = most_similar(keyword)
    similar_items = similar_items[:5]    
    for item in similar_items:
        data = recursive_get_similar_items(keyword=item[0], cur_score = item[1], cur_dept = cur_dept, max_dept=max_dept)
        res["children"].append(data)
    
    return res

def visualize(input, dept = 2):
    visualize_data = {}

    if len(input) == 0:
        return {}

    if len(input) == 1:
        visualize_data = recursive_get_similar_items(keyword=input[0], cur_score=1, cur_dept=0, max_dept=dept)
        
    
    if len(input) > 1:
        visualize_data = {
            "name": "Request Items",
            "data": {
                "score": 1
            },
            "children": []
        }
        for item in input:
            similar_items = recursive_get_similar_items(keyword=item, cur_score=1, cur_dept=1, max_dept=dept+1)
            visualize_data["children"].append(similar_items)
            
    print(visualize_data)

    visualize_data_path = Path(__file__).parent.parent.absolute() / 'related-skills-visualization' / 'skillTree.json'
    with open(visualize_data_path, mode='w', encoding='utf-8') as f:
        json.dump(visualize_data, f)

    # path = Path(__file__).parent.parent.absolute() / 'related-skills-visualization' / 'skill2vec_similar_skills.html'
    # webbrowser.register(
    #     'chrome',
    #     None,
	#     webbrowser.BackgroundBrowser("C://Program Files (x86)//Google//Chrome//Application//chrome.exe")
    # )
    # webbrowser.get('chrome').open(path)

visualize(input=["MVC"], dept=2)
